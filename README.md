OpenAPI Utilities
=================

Utilities helping with handling OpenAPI schemas. This uses jackson for
serialization and uses the swagger models as base for both OpenAPI and
JsonSchema serialization.

One of the complications for deserializing JsonSchema and it's more widely
used cousin OpenAPI is that it has fields with multiple possible value types,

- [json-schema](https://json-schema.org/) is the base for most of the extra
  features in comparison with OpenAPI.
- [Swagger](https://swagger.io/docs/specification/about/) the Swagger spec to
  be compatible with.