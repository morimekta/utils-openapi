package net.morimekta.openapi;

import io.swagger.v3.oas.models.OpenAPI;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.stream.Stream;

import static java.nio.charset.StandardCharsets.UTF_8;
import static net.morimekta.openapi.OpenAPIUtils.parseOpenAPIJson;
import static net.morimekta.openapi.OpenAPIUtils.parseOpenAPIYaml;
import static net.morimekta.openapi.OpenAPIUtils.toJson;
import static net.morimekta.openapi.OpenAPIUtils.toYaml;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class OpenAPIUtilsTest {
    public static Stream<Arguments> testParseData() {
        return Stream.of(
                arguments("openapi"),
                arguments("array"),
                arguments("composite"));
    }

    @ParameterizedTest
    @MethodSource("testParseData")
    public void testParse(String name) throws IOException {
        String yaml = resource("yaml/" + name + ".yaml");
        String json = resource("json/" + name + ".json");

        OpenAPI fromYaml = parseOpenAPIYaml(yaml);
        assertThat(toJson(fromYaml), is(json));

        OpenAPI fromJson = parseOpenAPIJson(json);
        assertThat(toYaml(fromJson), is(yaml));
    }

    private static String resource(String name) throws IOException {
        try (InputStream is = OpenAPIUtilsTest.class.getResourceAsStream("/net/morimekta/openapi/" + name)) {
            if (is == null) {
                throw new AssertionError("no such resource: " + name);
            }
            return new String(new BufferedInputStream(is).readAllBytes(), UTF_8);
        }
    }
}
