package net.morimekta.openapi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.PathItem;
import net.morimekta.openapi.jackson.OpenAPIModule;

import java.util.Map;
import java.util.TreeMap;

import static java.util.Objects.requireNonNull;

public class OpenAPIUtils {
    private static final System.Logger LOGGER = System.getLogger(OpenAPIUtils.class.getName());
    private static final ObjectMapper JSON;
    private static final ObjectMapper YAML;

    static {
        JSON = new ObjectMapper();
        JSON.registerModule(new OpenAPIModule());
        ObjectMapper yaml;
        try {
            com.fasterxml.jackson.dataformat.yaml.YAMLFactory yamlFactory = new com.fasterxml.jackson.dataformat.yaml.YAMLFactory();
            yamlFactory.configure(com.fasterxml.jackson.dataformat.yaml.YAMLGenerator.Feature.MINIMIZE_QUOTES, true);
            yamlFactory.configure(com.fasterxml.jackson.dataformat.yaml.YAMLGenerator.Feature.WRITE_DOC_START_MARKER, false);
            yamlFactory.configure(com.fasterxml.jackson.dataformat.yaml.YAMLGenerator.Feature.USE_NATIVE_OBJECT_ID, false);
            yamlFactory.configure(com.fasterxml.jackson.dataformat.yaml.YAMLGenerator.Feature.USE_NATIVE_TYPE_ID, false);
            yaml = new ObjectMapper(yamlFactory);
            yaml.registerModule(new OpenAPIModule());
        } catch (Throwable e) {
            LOGGER.log(System.Logger.Level.ERROR,
                       "Failed to instantiate YAML factory",
                       e);
            yaml = null;
        }
        YAML = yaml;
    }

    public static OpenAPI parseOpenAPIYaml(String yaml) {
        requireNonNull(yaml, "yaml == null");
        try {
            return YAML.readValue(yaml, OpenAPI.class);
        } catch (JsonProcessingException e) {
            throw new AssertionError(e.getMessage(), e);
        }
    }

    public static OpenAPI parseOpenAPIJson(String json) {
        requireNonNull(json, "json == null");
        try {
            return JSON.readValue(json, OpenAPI.class);
        } catch (JsonProcessingException e) {
            throw new AssertionError(e.getMessage(), e);
        }
    }

    /**
     * Normalize openAPI definition by sorting main known maps. This should make
     * the resulting definition consequentially ordered and thus testable.
     *
     * @param openAPI The OpenAPI definition to normalize.
     * @return The normalized definition. Same instance and passed.
     */
    public static OpenAPI normalizeOpenAPI(OpenAPI openAPI) {
        requireNonNull(openAPI, "openAPI == null");
        Map<String, PathItem> paths = new TreeMap<>(openAPI.getPaths());
        openAPI.getPaths().clear();
        openAPI.getPaths().putAll(paths);

        Components components = openAPI.getComponents();
        if (components.getSchemas() != null) {
            components.setSchemas(new TreeMap<>(components.getSchemas()));
        }
        if (components.getParameters() != null) {
            components.setParameters(new TreeMap<>(components.getParameters()));
        }
        if (components.getResponses() != null) {
            components.setResponses(new TreeMap<>(components.getResponses()));
        }
        if (components.getCallbacks() != null) {
            components.setCallbacks(new TreeMap<>(components.getCallbacks()));
        }
        if (components.getRequestBodies() != null) {
            components.setRequestBodies(new TreeMap<>(components.getRequestBodies()));
        }
        if (components.getExtensions() != null) {
            components.setExtensions(new TreeMap<>(components.getExtensions()));
        }
        if (components.getHeaders() != null) {
            components.setHeaders(new TreeMap<>(components.getHeaders()));
        }
        if (components.getLinks() != null) {
            components.setLinks(new TreeMap<>(components.getLinks()));
        }
        if (components.getSecuritySchemes() != null) {
            components.setSecuritySchemes(new TreeMap<>(components.getSecuritySchemes()));
        }
        return openAPI;
    }

    public static String toYaml(OpenAPI openAPI) {
        requireNonNull(openAPI, "openAPI == null");
        try {
            return YAML.writerWithDefaultPrettyPrinter().writeValueAsString(openAPI);
        } catch (JsonProcessingException e) {
            throw new AssertionError(e.getMessage(), e);
        }
    }

    public static String toJson(OpenAPI openAPI) {
        requireNonNull(openAPI, "openAPI == null");
        try {
            return JSON.writerWithDefaultPrettyPrinter().writeValueAsString(openAPI);
        } catch (JsonProcessingException e) {
            throw new AssertionError(e.getMessage(), e);
        }
    }
}
