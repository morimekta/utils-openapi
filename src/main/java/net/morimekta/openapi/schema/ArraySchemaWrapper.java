package net.morimekta.openapi.schema;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import io.swagger.v3.oas.models.media.ArraySchema;
import io.swagger.v3.oas.models.media.Schema;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_ABSENT)
@JsonDeserialize(using = ArraySchemaWrapper.Deserializer.class)
public class ArraySchemaWrapper extends ArraySchema {
    private Object additionalItems;

    public Object getAdditionalItems() {
        return additionalItems;
    }

    public void setAdditionalItems(Object additionalItems) {
        this.additionalItems = additionalItems;
    }

    // ---- Object ----

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ArraySchemaWrapper that = (ArraySchemaWrapper) o;
        return Objects.equals(additionalItems, that.additionalItems);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), additionalItems);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ArraySchemaWrapper {\n");
        sb.append("    ").append(toIndentedString(super.toString())).append("\n");
        sb.append("    additionalItems: ").append(toIndentedString(additionalItems)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    public static class Deserializer extends JsonDeserializer<Schema<?>> {
        @Override
        public Schema<?> deserialize(JsonParser jsonParser,
                                     DeserializationContext deserializationContext)
                throws IOException {
            Schema<?> schema = new ArraySchemaWrapper();
            String field = jsonParser.currentName();
            while (field != null){
                switch (field) {
                    case "items": {
                        if (jsonParser.nextToken() == JsonToken.START_ARRAY) {
                            CompactObjectSchema object = new CompactObjectSchema();
                            object.setMinItems(schema.getMinItems());
                            object.setMaxItems(schema.getMaxItems());
                            // compact object.
                            object.setItems(new ArrayList<>());
                            while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
                                object.getItems().add(deserializationContext.readValue(jsonParser, Schema.class));
                            }
                            schema = object;
                        } else {
                            // This should not be possible to fail on valid YAML or JSON, as the
                            // key 'items' should only appear once.
                            assert schema instanceof ArraySchemaWrapper : "Schema not an array";
                            ArraySchemaWrapper arraySchema = (ArraySchemaWrapper) schema;
                            arraySchema.setItems(deserializationContext.readValue(jsonParser, Schema.class));
                        }
                        break;
                    }
                    case "additionalItems":
                        Object additionalItems;
                        if (jsonParser.nextToken() == JsonToken.START_OBJECT) {
                            additionalItems = deserializationContext.readValue(jsonParser, Schema.class);
                        } else {
                            additionalItems = jsonParser.getBooleanValue();
                        }

                        if (schema instanceof CompactObjectSchema) {
                            ((CompactObjectSchema) schema).setAdditionalItems(additionalItems);
                        } else {
                            ((ArraySchemaWrapper) schema).setAdditionalItems(additionalItems);
                        }
                        break;
                    case "maxItems":
                        schema.setMaxItems(jsonParser.nextIntValue(0));
                        break;
                    case "minItems":
                        schema.setMinItems(jsonParser.nextIntValue(0));
                        break;
                    case "uniqueItems":
                        schema.setUniqueItems(jsonParser.nextBooleanValue());
                        break;
                    case "title":
                        schema.setTitle(jsonParser.nextTextValue());
                        break;
                    case "description":
                        schema.setDescription(jsonParser.nextTextValue());
                        break;
                    case "example":
                        schema.setExample(jsonParser.nextValue());
                        break;
                    // ------------
                    default:
                        throw new UnrecognizedPropertyException(
                                jsonParser,
                                "Invalid ArraySchema property: " + field,
                                jsonParser.getCurrentLocation(),
                                ArraySchemaWrapper.class,
                                field,
                                List.of("type",
                                        "items",
                                        "additionalItems",
                                        "maxItems",
                                        "minItems",
                                        "uniqueItems",
                                        "title",
                                        "description",
                                        "example"));
                }
                field = jsonParser.nextFieldName();
            }

            return schema;
        }
    }
}
