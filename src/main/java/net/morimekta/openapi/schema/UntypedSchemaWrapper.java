package net.morimekta.openapi.schema;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.InvalidTypeIdException;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import io.swagger.v3.oas.models.media.ComposedSchema;
import io.swagger.v3.oas.models.media.Schema;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@SuppressWarnings("rawtypes,unused")
@JsonDeserialize(using = UntypedSchemaWrapper.Deserializer.class)
public class UntypedSchemaWrapper extends Schema {
    private UntypedSchemaWrapper() {}

    public static class Deserializer extends JsonDeserializer<Schema<?>> {
        @Override
        public Schema<?> deserialize(JsonParser jsonParser,
                                     DeserializationContext deserializationContext)
                throws IOException {
            switch (jsonParser.currentName()) {
                case "$ref": return new Schema<>().$ref(jsonParser.nextTextValue());
                case "oneOf": return new ComposedSchema().oneOf(schemaList(jsonParser));
                case "anyOf": return new ComposedSchema().anyOf(schemaList(jsonParser));
                case "allOf": return new ComposedSchema().allOf(schemaList(jsonParser));
                case "type":
                    String value = jsonParser.nextTextValue();
                    JavaType schema = deserializationContext.constructType(Schema.class);
                    throw new InvalidTypeIdException(
                            jsonParser,
                            "Unknown schema type: " + value,
                            schema,
                            value);
                default:
                    throw new UnrecognizedPropertyException(
                            jsonParser,
                            "Invalid Untyped Schema property: " + jsonParser.currentName(),
                            jsonParser.getCurrentLocation(),
                            UntypedSchemaWrapper.class,
                            jsonParser.currentName(),
                            List.of("$ref",
                                    "oneOf",
                                    "anyOf",
                                    "allOf"));
            }
        }

        private List<Schema> schemaList(JsonParser jsonParser) throws IOException {
            List<Schema> out = new ArrayList<>();
            if (jsonParser.nextToken() == JsonToken.START_ARRAY) {
                while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
                    out.add(jsonParser.readValueAs(Schema.class));
                }
            } else {
                throw new InvalidFormatException(
                        jsonParser,
                        null,
                        "Invalid schema list: " + jsonParser.currentName(),
                        List.class);
            }
            return out;
        }
    }
}
