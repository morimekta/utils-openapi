package net.morimekta.openapi.schema;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.models.media.Schema;

import java.util.Map;
import java.util.TreeMap;

@SuppressWarnings("unused,rawtypes")
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class JsonSchemaModel {
    public static final String REF_PREFIX = "#/definitions/";

    private String              $schema = "http://json-schema.org/schema#";
    private String              $id;
    private String              title;
    private Map<String, Schema> definitions = new TreeMap<>();

    public String get$schema() {
        return $schema;
    }

    /**
     * URL to the json-schema of the json-schema version used.
     * @param $schema The new schema.
     */
    public void set$schema(String $schema) {
        this.$schema = $schema;
    }

    public String get$id() {
        return $id;
    }

    /**
     * URL to this json-schema definition.
     * @param $id The new ID.
     */
    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Map<String, Schema> getDefinitions() {
        return definitions;
    }

    public void setDefinitions(Map<String, Schema> definitions) {
        this.definitions = definitions;
    }

    // ---- Object ----
}
