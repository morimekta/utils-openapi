package net.morimekta.openapi.schema;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.swagger.v3.oas.models.media.BooleanSchema;
import io.swagger.v3.oas.models.media.IntegerSchema;
import io.swagger.v3.oas.models.media.NumberSchema;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.media.StringSchema;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        defaultImpl = UntypedSchemaWrapper.class,
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "type"
)
@JsonSubTypes({
        @Type(value = IntegerSchema.class, name = "integer"),
        @Type(value = StringSchema.class, name = "string"),
        @Type(value = NumberSchema.class, name = "number"),
        @Type(value = BooleanSchema.class, name = "boolean"),

        @Type(value = ArraySchemaWrapper.class, name = "array"),
        @Type(value = ObjectSchemaWrapper.class, name = "object"),
})
@JsonInclude(JsonInclude.Include.NON_NULL)
@SuppressWarnings("rawtypes")
public class SchemaWrapper extends Schema {
    private SchemaWrapper() {}
}
