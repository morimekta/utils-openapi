package net.morimekta.openapi.schema;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.models.media.Schema;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@SuppressWarnings("unused,rawtypes")
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class CompactObjectSchema extends Schema<Object> {
    private List<Schema> items;
    private Object additionalItems;

    public CompactObjectSchema() {
        super("array", null);
        items = new ArrayList<>();
    }

    public List<Schema> getItems() {
        return items;
    }

    public void setItems(List<Schema> items) {
        this.items = items;
    }

    public Object getAdditionalItems() {
        return additionalItems;
    }

    public void setAdditionalItems(Object additionalItems) {
        this.additionalItems = additionalItems;
    }

    // ---- Object ----

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        CompactObjectSchema that = (CompactObjectSchema) o;
        return Objects.equals(items, that.items) &&
               Objects.equals(additionalItems, that.additionalItems);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), items, additionalItems);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class CompactObjectSchema {\n");
        sb.append("    ").append(toIndentedString(super.toString())).append("\n");
        sb.append("    additionalItems: ").append(toIndentedString(additionalItems)).append("\n");
        sb.append("}");
        return sb.toString();
    }
}
