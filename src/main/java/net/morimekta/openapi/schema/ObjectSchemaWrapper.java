package net.morimekta.openapi.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.models.media.ObjectSchema;
import io.swagger.v3.oas.models.media.Schema;

@SuppressWarnings("unused,rawtypes")
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class ObjectSchemaWrapper extends ObjectSchema {
    public ObjectSchemaWrapper() {
    }

    @JsonProperty("additionalProperties")
    public void setAdditionalProperties(Schema additionalProperties) {
        setAdditionalProperties((Object) additionalProperties);
    }

    @JsonProperty("additionalProperties")
    public void setAdditionalProperties(boolean additionalProperties) {
        setAdditionalProperties((Object) additionalProperties);
    }

    @Override
    @JsonIgnore
    public void setAdditionalProperties(Object additionalProperties) {
        super.setAdditionalProperties(additionalProperties);
    }
}
