package net.morimekta.openapi.jackson;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.module.SimpleModule;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.Paths;
import io.swagger.v3.oas.models.callbacks.Callback;
import io.swagger.v3.oas.models.examples.Example;
import io.swagger.v3.oas.models.headers.Header;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.links.Link;
import io.swagger.v3.oas.models.links.LinkParameter;
import io.swagger.v3.oas.models.media.ArraySchema;
import io.swagger.v3.oas.models.media.BinarySchema;
import io.swagger.v3.oas.models.media.BooleanSchema;
import io.swagger.v3.oas.models.media.ByteArraySchema;
import io.swagger.v3.oas.models.media.ComposedSchema;
import io.swagger.v3.oas.models.media.Content;
import io.swagger.v3.oas.models.media.DateSchema;
import io.swagger.v3.oas.models.media.DateTimeSchema;
import io.swagger.v3.oas.models.media.Discriminator;
import io.swagger.v3.oas.models.media.EmailSchema;
import io.swagger.v3.oas.models.media.Encoding;
import io.swagger.v3.oas.models.media.EncodingProperty;
import io.swagger.v3.oas.models.media.FileSchema;
import io.swagger.v3.oas.models.media.IntegerSchema;
import io.swagger.v3.oas.models.media.MapSchema;
import io.swagger.v3.oas.models.media.MediaType;
import io.swagger.v3.oas.models.media.NumberSchema;
import io.swagger.v3.oas.models.media.ObjectSchema;
import io.swagger.v3.oas.models.media.PasswordSchema;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.media.StringSchema;
import io.swagger.v3.oas.models.media.UUIDSchema;
import io.swagger.v3.oas.models.media.XML;
import io.swagger.v3.oas.models.parameters.CookieParameter;
import io.swagger.v3.oas.models.parameters.HeaderParameter;
import io.swagger.v3.oas.models.parameters.Parameter;
import io.swagger.v3.oas.models.parameters.PathParameter;
import io.swagger.v3.oas.models.parameters.QueryParameter;
import io.swagger.v3.oas.models.parameters.RequestBody;
import io.swagger.v3.oas.models.responses.ApiResponse;
import io.swagger.v3.oas.models.responses.ApiResponses;
import io.swagger.v3.oas.models.security.OAuthFlow;
import io.swagger.v3.oas.models.security.OAuthFlows;
import io.swagger.v3.oas.models.security.Scopes;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.servers.Server;
import io.swagger.v3.oas.models.servers.ServerVariable;
import io.swagger.v3.oas.models.servers.ServerVariables;
import io.swagger.v3.oas.models.tags.Tag;
import net.morimekta.openapi.schema.SchemaWrapper;

public class OpenAPIModule extends SimpleModule {
    public OpenAPIModule() {
        super("OpenAPI");
        setMixInAnnotation(Schema.class, SchemaWrapper.class);
    }

    @Override
    public void setupModule(SetupContext context) {
        super.setupModule(context);
        setIncludeNonNull(context,
                          // And core classes
                          ApiResponse.class,
                          ApiResponses.class,
                          Callback.class,
                          Components.class,
                          Contact.class,
                          Contact.class,
                          CookieParameter.class,
                          Example.class,
                          ExternalDocumentation.class,
                          Header.class,
                          HeaderParameter.class,
                          Info.class,
                          License.class,
                          Link.class,
                          LinkParameter.class,
                          MediaType.class,
                          OAuthFlow.class,
                          OAuthFlows.class,
                          OpenAPI.class,
                          Operation.class,
                          Parameter.class,
                          PathItem.class,
                          PathParameter.class,
                          Paths.class,
                          QueryParameter.class,
                          RequestBody.class,
                          Schema.class,
                          Scopes.class,
                          SecurityRequirement.class,
                          SecurityScheme.class,
                          Server.class,
                          ServerVariable.class,
                          ServerVariables.class,
                          Tag.class,
                          // --- schema itself
                          ArraySchema.class,
                          BinarySchema.class,
                          BooleanSchema.class,
                          ByteArraySchema.class,
                          ComposedSchema.class,
                          Content.class,
                          DateSchema.class,
                          DateTimeSchema.class,
                          Discriminator.class,
                          EmailSchema.class,
                          Encoding.class,
                          EncodingProperty.class,
                          FileSchema.class,
                          IntegerSchema.class,
                          MapSchema.class,
                          MediaType.class,
                          NumberSchema.class,
                          ObjectSchema.class,
                          PasswordSchema.class,
                          Schema.class,
                          StringSchema.class,
                          UUIDSchema.class,
                          XML.class);
    }

    private static void setIncludeNonNull(SetupContext context, Class<?>... types) {
        JsonInclude.Value includeNonNull = new JsonInclude.Value(SchemaWrapper.class.getAnnotation(JsonInclude.class));
        for (Class<?> type : types) {
            context.configOverride(type).setInclude(includeNonNull);
        }
    }
}
